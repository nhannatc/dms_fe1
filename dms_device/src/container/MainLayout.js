import React from 'react'
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import Sidebar from '../components/Sider/Sidebar';
import DinamicHeader from '../components/Header/Header';


export default function MainLayout(props) {
    const { Content, Footer, Sider } = Layout;
    return (
        <Layout>
            <DinamicHeader />
            <Layout>
                <Sider>
                    <Sidebar key='1' />
                </Sider>
                <Content style={{ margin: '24px 16px 0' }}>
                    <div className="site-layout-background" style={{ padding: 24, minHeight: 360, background: "white" }}>
                        {props.children}
                    </div>
                </Content>
            </Layout>
            <Layout>
                <Footer style={{ textAlign: 'center' }}>Devices manage 2021 @Vo Nguyen Thinh</Footer>
            </Layout>
        </Layout>
    )
}
