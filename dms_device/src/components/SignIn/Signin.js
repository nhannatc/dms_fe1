import React from 'react'
import auth from './auth';
// ----------------------------------------------------------------------------
import 'antd/dist/antd.css'
import { Row, Col } from 'antd';
import { Form, Input, Button, Divider } from 'antd';
import { UserOutlined, LockOutlined, KeyOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
// -----------------------------------------------------------------------------

export default function Signin(props) {
    const history = useHistory()
    const login = () => {
        auth.login(() => {
            localStorage.setItem("isUser", "true")
            localStorage.setItem("role", "admin")
            alert("Login Success")
            history.push('/db/devices')
        })
    }
    return (
        <div className='container'>
            <div className='login-fomrm'>
                <Row align="middle" style={{ height: '935px', background: '#fafafb' }}>
                    <Col xs={2} sm={4} md={6} lg={8} xl={9} span={9} />

                    <Col style={{
                        background: 'white',
                        boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
                        borderRadius: '7px',
                        padding: '50px 30px 50px 30px',
                    }}
                        xs={20} sm={16} md={12} lg={8} xl={6} span={6}>

                        <Divider plain>LOGIN FORM</Divider>
                        <Form
                        >
                            <Form.Item
                                name="username"
                                rules={[{ required: false, message: 'Please input your username!' }]}
                            >
                                <Input size='large' placeholder="Username" prefix={<UserOutlined />} />
                            </Form.Item>

                            <Form.Item
                                name="password"
                                rules={[{ required: false, message: 'Please input your password!' }]}
                            >
                                <Input.Password size='large' placeholder="Password" prefix={<LockOutlined />} />
                            </Form.Item>

                            <Form.Item>
                                <Button onClick={login} block size='large' type="primary" htmlType="submit" icon={<KeyOutlined />}>
                                    LOGIN
                                </Button>



                            </Form.Item>

                        </Form>
                    </Col>

                    <Col xs={2} sm={4} md={6} lg={8} xl={9} span={9} />
                </Row>
            </div>
        </div >
    )
}
