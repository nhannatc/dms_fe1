import React from 'react'
import { Link } from 'react-router-dom'
import { Menu } from 'antd';
import { LaptopOutlined, LineChartOutlined, UserOutlined, ToolOutlined, HistoryOutlined } from '@ant-design/icons';
export default function Sidebar(props) {

    const isRole = localStorage.getItem('role')
    const renderItem = () => {
        if (isRole === 'admin') {
            return (
                <Menu theme='dark' defaultSelectedKeys={[1]}>
                    <Menu.Item key='1' icon={<LaptopOutlined />}>
                        <Link to='/db/devices'>Devices</Link>
                    </Menu.Item>
                    <Menu.Item key='2' icon={<ToolOutlined />}>
                        <Link to='/db/manage-devices'>Manage Devices</Link>
                    </Menu.Item>
                    <Menu.Item key='3' icon={<UserOutlined />}>
                        <Link to='/db/manage-users'>Manage User</Link>
                    </Menu.Item>
                    <Menu.Item key='4' icon={<LineChartOutlined />}>
                        <Link to='/db/tracking'>Devices traking</Link>
                    </Menu.Item>
                </Menu>
            )
        } else {
            return (
                <Menu theme='dark' defaultSelectedKeys={[1]}>
                    <Menu.Item key='1' icon={<LineChartOutlined />}>
                        <Link to='/db/devices'>Devices</Link>
                    </Menu.Item>
                    <Menu.Item key='2' icon={<HistoryOutlined />}>
                        <Link to='/db/devices-history'>Devices traking</Link>
                    </Menu.Item>
                </Menu>
            )
        }
    }

    return (
        renderItem(props.role)
    )
}
