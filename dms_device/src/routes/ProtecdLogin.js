import React from 'react'
import { Route, Redirect } from 'react-router-dom'
export const ProtectedLogin = ({ component: Component, ...rest }) => {
    const user = localStorage.getItem('isUser')
    return (
        <Route {...rest} render={props => {
            if (!user) {
                return <Component {...props} />
            } else {
                return <Redirect to={
                    {
                        pathname: '/db/devices',
                        state: {
                            from: props.location
                        }
                    }
                }
                />
            }
        }} />
    )
}



