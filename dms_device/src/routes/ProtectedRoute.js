import React from 'react'
import { Route, Redirect, useLocation } from 'react-router-dom'
export const ProtectedRoute = ({ component: Component, ...rest }) => {
    const user = localStorage.getItem('isUser')
    const location = useLocation()
    return (
        <Route {...rest} render={props => {
            if (user) {
                return <Component {...props} />
            } else {
                return <Redirect to={
                    {
                        pathname: '/login',
                        state: {
                            from: location
                        }
                    }
                }
                />
            }
        }} />
    )
}



