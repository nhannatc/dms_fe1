import React from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
// ---------------------------------------------------------------------------
import Singin from '../components/SignIn/Signin'
import MainLayout from '../container/MainLayout'
import Devices from '../components/Devices/Device'
import MaDevices from '../components/Devices/MaDevices'
import Tracking from '../components/Devices/Tracking'
import MaUsers from '../components/User/MaUsers'
import DeviHistory from '../components/Devices/DeviHistory'
import { ProtectedRoute } from '../routes/ProtectedRoute'
import { ProtectedLogin } from './ProtecdLogin'
// ------------------------------------------------------------------------------
export default function MainRouter() {
    return (
        <Router>
            <Switch>
                <ProtectedLogin exact path='/' component={Singin} />
                <ProtectedLogin exact path='/login' component={Singin} />
                <ProtectedRoute path='/db'>
                    <MainLayout>
                        <Switch>
                            <ProtectedRoute path='/db/devices' component={Devices} />
                            {/* --------------ADMIN ROUTER--------------------- */}
                            <ProtectedRoute path='/db/manage-devices' component={MaDevices} />
                            <ProtectedRoute path='/db/manage-users' component={MaUsers} />
                            <ProtectedRoute path='/db/tracking' component={Tracking} />
                            {/* ---------------USER ROUTER------------------------ */}
                            <ProtectedRoute path='/db/devives' component={Devices} />
                            <ProtectedRoute path='/db/devices-history' component={DeviHistory} />
                        </Switch>
                    </MainLayout>
                </ProtectedRoute>
            </Switch>
        </Router>
    )
}
